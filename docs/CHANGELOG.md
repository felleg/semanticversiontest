# [2.3.0](https://gitlab.com/felleg/semanticversiontest/compare/v2.2.0...v2.3.0) (2024-01-09)


### Features

* **gitlab-ci.yml:** update node version ([e9c8664](https://gitlab.com/felleg/semanticversiontest/commit/e9c8664615e1728c5bf50c8560a65de2a06b77fd))

# [2.2.0](https://gitlab.com/felleg/semanticversiontest/compare/v2.1.0...v2.2.0) (2020-03-04)


### Bug Fixes

* **architecture:** just a little fix before going to production ([0d33202](https://gitlab.com/felleg/semanticversiontest/commit/0d3320233ba6512b4f476453f3b7aa92f8f91f32))


### Features

* **general:** i added a new feature ([98c4db7](https://gitlab.com/felleg/semanticversiontest/commit/98c4db7db0d5d14a765539fdc24344e613c7340c))

# [2.1.0](https://gitlab.com/felleg/semanticversiontest/compare/v2.0.2...v2.1.0) (2020-03-04)


### Features

* yesh! a feature ([5df6fe3](https://gitlab.com/felleg/semanticversiontest/commit/5df6fe3fc84db9b072b232671871412d8cdd2a15))
